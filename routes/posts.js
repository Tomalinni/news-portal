var express = require("express");
var router = express.Router();
var Post = require("../models/post");
var middleware = require("../middleware");

// INDEX - show all Posts
router.get("/", function(req, res){
    console.log(req.user);
    // Get all Posts from DB
    Post.find({}, function(err, allPosts){
        if(err){
            console.log(err);
        } else {
            res.render("posts/index", {posts: allPosts});
        }
    });
});

router.get("/transport", function(req, res){
    console.log(req.user);
    // Get all Posts from DB
    Post.find({}, function(err, allPosts){
        if(err){
            console.log(err);
        } else {
            res.render("posts/transport", {posts: allPosts});
        }
    });
});

router.get("/animals", function(req, res){
    console.log(req.user);
    // Get all Posts from DB
    Post.find({}, function(err, allPosts){
        if(err){
            console.log(err);
        } else {
            res.render("posts/animals", {posts: allPosts});
        }
    });
});

router.get("/food", function(req, res){
    console.log(req.user);
    // Get all Posts from DB
    Post.find({}, function(err, allPosts){
        if(err){
            console.log(err);
        } else {
            res.render("posts/food", {posts: allPosts});
        }
    });
});

router.get("/sport", function(req, res){
    console.log(req.user);
    // Get all Posts from DB
    Post.find({}, function(err, allPosts){
        if(err){
            console.log(err);
        } else {
            res.render("posts/sport", {posts: allPosts});
        }
    });
});

router.get("/featured", function(req, res){
    console.log(req.user);
    // Get all Posts from DB
    Post.find({}, function(err, allPosts){
        if(err){
            console.log(err);
        } else {
            res.render("posts/featured", {posts: allPosts});
        }
    });
});

// CREATE - add new Post to DB
router.post("/", middleware.isLoggedIn, function(req, res){
  var type = req.body.type  
  var name = req.body.name;
  var image = req.body.image;
  var description = req.body.description;
  var author = {
    id: req.user._id,
    username: req.user.username
  };
  var newPost = {type: type, name: name, image: image, description: description, author: author };
  // Create a new Post and save to DB
  Post.create(newPost, function(err, newlyCreated){
    if(err){
        console.log(err);
    } else {
        // redirect back to Posts page
        res.redirect("/posts");
    }
  })
});

// SHOW - show from to create new Post 
router.get("/new", middleware.isLoggedIn, function(req, res){
    res.render("posts/new");
}),

// SHOW - shows more info about one Post
router.get("/:id", function(req, res) {
    // find the Post with provided ID
    Post.findById(req.params.id).populate("comments").exec(function(err, foundPost){
        if(err){
            console.log(err);
        } else {
            console.log(foundPost);
            // render show template with that Post
            res.render("posts/show", {post: foundPost});
        }
    });  
});

// EDIT Post ROUTE
router.get("/:id/edit", middleware.checkPostOwnership, function(req, res){
    Post.findById(req.params.id, function(err, foundPost){
        res.render("posts/edit", {post: foundPost});
    });
});

//UPDATE Post ROUTE
router.put("/:id", middleware.checkPostOwnership, function(req, res){
    // find and update the correct Post
    Post.findByIdAndUpdate(req.params.id, req.body.post, function(err, updatedPost){
        if(err){
            res.redirect("/posts");
        } else {
            res.redirect("/posts/" + req.params.id);
        }
    })
    // redirect somewhere (show page)
})

// DESROY Post ROUTE
router.delete("/:id", middleware.checkPostOwnership, function(req, res){
 Post.findByIdAndRemove(req.params.id, function(err){
     if(err){
         res.redirect("/Posts");
     } else {
        //  alert("You are deleted one item");
         res.redirect("/Posts");
     }
 });
});

module.exports = router;