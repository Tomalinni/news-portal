var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var passport = require("passport");
var LocalStrategy = require("passport-local");
var methoOverride = require("method-override");
var Post = require("./models/post");
var Comment = require("./models/comment");
var User = require("./models/user");
var seedDB = require("./seeds");

// requring routes
var commentRoutes = require("./routes/comments"),
    postRoutes = require("./routes/posts"),
    authRoutes = require("./routes/index");


mongoose.connect("mongodb://localhost/news_app6");
app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(methoOverride("_method"));
// seed the database
seedDB();

// PASSPORT REGISTRATION
app.use(require("express-session")({
    secret: "Once",
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use(function(req, res, next){
res.locals.currentUser = req.user;
next();
});

app.use("/posts/:id/comments", commentRoutes);
app.use("/posts", postRoutes);
app.use("/", authRoutes);

app.listen(3000, process.env.IP, function(){
    console.log("Server Has Started");
});
